/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.masivian;

/**
 * Clase que permite cargar los datos de la pagina
 * 
 * @author carlos
 */
class Pagina {
    
    private int m;
    private int rr;
    private int cc;
    private int ordMax;
    private int p[];
    
    public Pagina(int M, int RR, int CC, int ordMax) {
        this.m = M;
        this.rr = RR;
        this.cc = CC;
        this.ordMax = ordMax;
        this.p = new int[M + 1];
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getRr() {
        return rr;
    }

    public void setRr(int rr) {
        this.rr = rr;
    }

    public int getCc() {
        return cc;
    }

    public void setCc(int cc) {
        this.cc = cc;
    }

    public int getOrdMax() {
        return ordMax;
    }

    public void setOrdMax(int ordMax) {
        this.ordMax = ordMax;
    }

    public int[] getP() {
        return p;
    }

    public void setP(int[] p) {
        this.p = p;
    }

    

}
