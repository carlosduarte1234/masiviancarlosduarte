/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.masivian;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que permite realizar una operacion de Printer y despues evaluar el
 * resultado
 *
 * @author carlos
 */
public class Printer {

    private Pagina pagina;

    /**
     * Metodo utilizado como constructor de todos los parametros requeridos
     *
     *
     * @param m
     * @param rr
     * @param cc
     * @param ordMax
     */
    public Printer(int m, int rr, int cc, int ordMax) {
        pagina = new Pagina(m, rr, cc, ordMax);
    }

    /**
     * Metodo encargado de realizar la operacion de Printer
     */
    public void operacion() {

        int J = 1;
        boolean JPRIME;
        int ORD;
        int SQUARE;
        int MULT[] = new int[getPagina().getOrdMax() + 1];
        getPagina().getP()[1] = 2;
        ORD = 2;
        SQUARE = 9;
        for (int K = 1; K < getPagina().getM(); K++) {
            do {
                J += 2;
                if (J == SQUARE) {
                    ORD++;
                    SQUARE = getPagina().getP()[ORD] * getPagina().getP()[ORD];
                    MULT[ORD - 1] = J;
                }
                JPRIME = true;
                for (int N = 2; N < ORD && JPRIME; N++) {
                    while (MULT[N] < J) {
                        MULT[N] += getPagina().getP()[N] + getPagina().getP()[N];
                    }
                    if (MULT[N] == J) {
                        JPRIME = false;
                    }
                }
            } while (!JPRIME);
            getPagina().getP()[K + 1] = J;
        }

    }

    /**
     * Metodo encargado de validar la operacion de Printer
     */
    public void checkOperacion() {

        int PAGENUMBER;
        int PAGEOFFSET;
        int ROWOFFSET;

        PAGENUMBER = 1;
        PAGEOFFSET = 1;

        if (PAGEOFFSET <= getPagina().getM()) {
            while (PAGEOFFSET <= getPagina().getM()) {
                Logger.getLogger(Printer.class.getName()).log(Level.INFO, "Error al ejecutar la operacion run");
                Logger.getLogger(Printer.class.getName()).log(Level.INFO, "The First " + Integer.toString(getPagina().getM()));
                Logger.getLogger(Printer.class.getName()).log(Level.INFO, " Prime Numbers === Page " + Integer.toString(PAGENUMBER));
                for (ROWOFFSET = PAGEOFFSET; ROWOFFSET <= PAGEOFFSET + getPagina().getRr() - 1; ROWOFFSET++) {
                    for (int C = 0; C <= getPagina().getCc() - 1; C++) {
                        if (ROWOFFSET + C * getPagina().getRr() <= getPagina().getM()) {
                            Logger.getLogger(Printer.class.getName()).log(Level.INFO, "" + getPagina().getP()[ROWOFFSET + C * getPagina().getRr()]);
                        }
                    }
                    Logger.getLogger(Printer.class.getName()).log(Level.INFO, "");
                }
                Logger.getLogger(Printer.class.getName()).log(Level.INFO, "\f");
                PAGENUMBER++;
                PAGEOFFSET += getPagina().getRr() * getPagina().getCc();
            }
        } else {
            Logger.getLogger(Printer.class.getName()).log(Level.INFO, "Ejecucion exitosa.");
        }
    }

    public Pagina getPagina() {
        return pagina;
    }

    public void setPagina(Pagina pagina) {
        this.pagina = pagina;
    }

    /**
     * Operacion principal para ejecutar la logica de negocio de la clase
     * Printer
     *
     * @param args
     */
    public static void main(String[] args) {

        try {
            Printer p = new Printer(Integer.valueOf(args[0]), Integer.valueOf(args[1]), Integer.valueOf(args[2]), Integer.valueOf(args[3]));
            p.operacion();
            p.checkOperacion();
        } catch (Exception e) {
        }

    }

}
